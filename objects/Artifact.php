<?php

class Artifact implements IPreviewable
{
    public $Id;
    public $Name;
    public $Content;
    public $Description;
    public $ContentTypeId;
    public $ContentTypeName;
    public $TemplateId;
    
    function Artifact($id, $name, $content, $description, $contentTypeId, $templateId)
    {
        $this->Id = $id;
        $this->Name = $name;
        $this->Content = $content;
        $this->Description = $description;
        $this->ContentTypeId = $contentTypeId;
        $this->ContentTypeName = DbHandler::GetContentTypeName($contentTypeId);
        $this->TemplateId = $templateId;
    }
    
    
    public function GetPreview()
    {
        switch ($this->ContentTypeId)
        {
            case 1:
                return $this->GetTextContent();
                break;
            case 2:
                return $this->GetImageContent();
                break;
        }
    }
    
    public function GetLocalPreview()
    {
        switch ($this->ContentTypeId)
        {
            case 1:
                return $this->GetTextContent();
                break;
            case 2:
                return $this->GetLocalImageContent();
                break;
        }
    }
    
    public function GetTextContent()
    {
        return "<p>" . $this->Content . "</p>"; 
    }
    
    public function GetImageContent()
    {
        return "<img  width=100px height=100px src=\"../" . $this->Content . "\">"; 
    }
    
    public function GetLocalImageContent()
    {
        return "<img  width=100px height=100px src=\"" . $this->Content . "\">"; 
    }
}

?>