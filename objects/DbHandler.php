<?php

class DbHandler
{
    const MYSQL_SERVER = "localhost";
    const MYSQL_USER = "root";
    const MYSQL_PASSWORD = "";
    const MYSQL_DB = "museum";
    
    static public $link;
    
    /// <summary>
    /// Connects to database.
    /// </summary>
    /// <returns>Database connection descriptor.</returns>
    public static function Db_connect()
    {
        self::$link = mysqli_connect(self::MYSQL_SERVER, self::MYSQL_USER, self::MYSQL_PASSWORD, self::MYSQL_DB)
            or die("Error: ".mysqli_error(self::$link));
        if(!mysqli_set_charset(self::$link, "utf8"))
        {
            printf("Error: ".mysqli_error(self::$link));
        }
    }
    
    /// <summary>
    /// Gets exposures from database.
    /// </summary>
    /// <returns>Array of Exposures.</returns>
    public static function GetExposures()
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM exposures 
                           ORDER BY likes DESC", "");
        
        $n = mysqli_num_rows($result);
        $exposures = array();
        
        for ($i = 0; $i < $n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            array_push($exposures, new Exposure($row['id'], $row['name'], $row['author'], $row['annotation'], $row['creationDate'], $row['likes'], $row['dislikes'], $row['previewImgSrc'], $row['mainImgSrc'], $row['templateId'], $row['framesId']));
        }
        
        return $exposures;
    }
    
    /// <summary>
    /// Gets artifacts from database.
    /// </summary>
    /// <returns>Array of Artifacts.</returns>
    public static function GetArtifacts()
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM artifacts 
                           ORDER BY name DESC", "");
        
        $n = mysqli_num_rows($result);
        $artifacts = array();
        
        for ($i = 0; $i < $n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            array_push($artifacts, new Artifact($row['id'], $row['name'], $row['content'], $row['description'], $row['content_type_id'], $row['templateId']));
        }
        
        return $artifacts;
    }
    
    /// <summary>
    /// Gets frames from database.
    /// </summary>
    /// <returns>Array of Frames.</returns>
    public static function GetFrames()
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM frames 
                           ORDER BY name DESC", "");
        
        $n = mysqli_num_rows($result);
        $frames = array();
        
        for ($i = 0; $i < $n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            array_push($frames, new FrameBase($row['id'], $row['name'], "", $row['artifact_id'], $row['content_type_id']));
        }
        
        return $frames;
    }
    
    /// <summary>
    /// Gets templates from database.
    /// </summary>
    /// <returns>Array of Templates.</returns>
    public static function GetTemplates()
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM templates", "");
        
        $n = mysqli_num_rows($result);
        $templates = array();
        
        for ($i = 0; $i < $n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            array_push($templates, new Template($row['id'], $row['name'], $row['path'] , $row['description']));
        }
        
        return $templates;
    }
    
    public static function GetContentTypes()
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM content_types", "");
        
        $n = mysqli_num_rows($result);
        $types = array();
        
        for ($i = 0; $i < $n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            array_push($types, new ContentType($row['id'], $row['name']));
        }
        
        return $types;
    }
    
    public static function GetContentTypeName($id_contentType)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM content_types WHERE id=%d", (int)$id_contentType);
        
        $type = mysqli_fetch_assoc($result);
        
        return $type['name'];
    }
    
    public static function GetContentType($id_contentType)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM content_types WHERE id=%d", (int)$id_contentType);
        
        $type = mysqli_fetch_assoc($result);
        
        return new ContentType($type['id'], $type['name']);
    }
    
    public static function GetArtifactName($id_aftifact)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM artifacts WHERE id=%d", (int)$id_aftifact);
        
        $type = mysqli_fetch_assoc($result);
        
        return $type['name'];
    }
    
    public static function GetContentTypeByName($typeName)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryStringResult("SELECT * FROM content_types WHERE name='%s'", $typeName);
        
        $contentType = mysqli_fetch_assoc($result);
        
        $type = new ContentType($contentType['id'], $contentType['name']);
        
        return $type;
    }
    
    /// <summary>
    /// Gets template by its name.
    /// </summary>
    /// <param name="$templateName">The name of template to get.</param>
    /// <returns>Single template.</returns>
    public static function GetTemplateByName($templateName)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();

        $result = self::GetQueryStringResult("SELECT * FROM templates WHERE name='%s'", $templateName);
        
        $template = mysqli_fetch_assoc($result);
        
        return new Template($template['id'], $template['name'], $template['path'], $template['description']);
    }
    
    public static function GetArtifactsByContentType($id_contentType)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryStringResult("SELECT * FROM artifacts WHERE content_type_id=%d", $id_contentType);
        
        $n = mysqli_num_rows($result);
        $artifacts = array();
        
        for ($i = 0; $i < $n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            array_push($artifacts, new Artifact($row['id'], $row['name'], $row['content'], $row['description'], $row['content_type_id'], $row['templateId']));
        }
        
        return $artifacts;
    }
    
    public static function GetFramesByName($name)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryStringResult("SELECT * FROM frames WHERE name='%s'", $name);
        
        $n = mysqli_num_rows($result);
        $frames = array();
        
        for ($i = 0; $i < $n; $i++)
        {
            $row = mysqli_fetch_assoc($result);
            array_push($frames, new FrameBase($row['id'], $row['name'], "", $row['artifact_id'], $row['content_type_id']));
        }
        
        return $frames;
    }
    
    /// <summary>
    /// Gets single exposure by its id.
    /// </summary>
    /// <param name="$id_exposure">The id of exposure to get.</param>
    /// <returns>Single exposure.</returns>
    public static function GetExposure($id_exposure)
    {    
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM exposures WHERE id=%d", (int)$id_exposure);
        
        $exposure = mysqli_fetch_assoc($result);
        
        return new Exposure($exposure['id'], $exposure['name'], $exposure['author'], $exposure['annotation'], $exposure['creationDate'], $exposure['likes'], $exposure['dislikes'], $exposure['previewImgSrc'], $exposure['previewImgSrc'], $exposure['templateId'], $exposure['framesId']);
        
    }
    
    /// <summary>
    /// Gets single exposure by its id.
    /// </summary>
    /// <param name="$id_exposure">The id of exposure to get.</param>
    /// <returns>Single exposure.</returns>
    public static function GetExposureByName($name)
    {    
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryStringResult("SELECT * FROM exposures WHERE name='%s'", $name);
        
        $exposure = mysqli_fetch_assoc($result);
        
        if($exposure == null)
            return null;
        else return new Exposure($exposure['id'], $exposure['name'], $exposure['author'], $exposure['annotation'], $exposure['creationDate'], $exposure['likes'], $exposure['dislikes'], $exposure['previewImgSrc'], $exposure['previewImgSrc'], $exposure['templateId'], $exposure['framesId']);
        
    }
    
    
    public static function NewExposure($exposure)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $t = "INSERT INTO exposures (id, name, author, annotation, creationDate, likes, dislikes, previewImgSrc, mainImgSrc, templateId, framesId) VALUES ('%i', '%s', '%s', '%s', now(), '%d', '%d', '%s', '%s', '%d', '%s')";

        $query = sprintf($t, mysqli_real_escape_string(self::$link, null),
                         mysqli_real_escape_string(self::$link, $exposure->name),
                         mysqli_real_escape_string(self::$link, null),
                         mysqli_real_escape_string(self::$link, $exposure->annotation),
                         mysqli_real_escape_string(self::$link, null),
                         mysqli_real_escape_string(self::$link, null),
                         mysqli_real_escape_string(self::$link, null),
                         mysqli_real_escape_string(self::$link, null),
                         mysqli_real_escape_string(self::$link, $exposure->templateId),
                        mysqli_real_escape_string(self::$link, null));
                
        $result = mysqli_query(self::$link, $query);
        
        if (!$result)
            die(mysqli_error(self::$link));
        
        return true;
        
    }
    
    public static function NewArtifact($artifact)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $t = "INSERT INTO artifacts (id, name, content, description, content_type_id, templateId) VALUES ('%d', '%s', '%s','%s', '%d','%d')";

        $query = sprintf($t, mysqli_real_escape_string(self::$link, $artifact->Id),
                         mysqli_real_escape_string(self::$link, $artifact->Name),
                         mysqli_real_escape_string(self::$link, $artifact->Content),
                         mysqli_real_escape_string(self::$link, $artifact->Description),
                         mysqli_real_escape_string(self::$link, $artifact->ContentTypeId),
                         mysqli_real_escape_string(self::$link, 1));
        
        $result = mysqli_query(self::$link, $query);
        
        if (!$result)
            die(mysqli_error(self::$link));
        
        return true;
        
    }
    
    public static function EditArtifact($artifact)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $t = "UPDATE artifacts name = '%s', content = '%s', content_type_id = '%d', templateId = '%d' WHERE id = '%d'";

        $query = sprintf($t, mysqli_real_escape_string(self::$link, $artifact->Name),
                         mysqli_real_escape_string(self::$link, $artifact->Content),
                         mysqli_real_escape_string(self::$link, $artifact->ContentTypeId),
                         mysqli_real_escape_string(self::$link, 1),
                         mysqli_real_escape_string(self::$link, $artifact->Id));
        
        $result = mysqli_query(self::$link, $query);
        
        if (!$result)
            die(mysqli_error(self::$link));
        
        return true;
        
    }
    
    public static function NewFrame($frame)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $t = "INSERT INTO frames (name, artifact_id, content_type_id) VALUES ('%s', '%d','%d')";

        $query = sprintf($t, 
                         mysqli_real_escape_string(self::$link, $frame->Name),
                         mysqli_real_escape_string(self::$link, $frame->ArtifactId),
                         mysqli_real_escape_string(self::$link, $frame->ContentTypeId));
        
        $result = mysqli_query(self::$link, $query);
        
        if (!$result)
            die(mysqli_error(self::$link));
        
        return true;
        
    }
    
    public static function EditExposure($exposure)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        

        $strFramesId = "[";
        $i = 0;
        foreach($exposure->arrFramesId as $frameId)
        {
            if ($i + 1 == count($exposure->arrFramesId)) $strFramesId = $strFramesId . $frameId . "]";
            else $strFramesId = $strFramesId . $frameId . ",";
            $i++;
        }
           
        $t = "UPDATE exposures SET framesId='%s', annotation='%s', previewImgSrc='%s', mainImgSrc='%s' WHERE id='%d'";
        
        $query = sprintf($t, 
                         mysqli_real_escape_string(self::$link, $strFramesId),
                         mysqli_real_escape_string(self::$link, $exposure->annotation),
                         mysqli_real_escape_string(self::$link, $exposure->previewImgSrc),
                         mysqli_real_escape_string(self::$link, $exposure->mainImgSrc),
                         mysqli_real_escape_string(self::$link, $exposure->id));
        
        $result = mysqli_query(self::$link, $query);
        
        if (!$result)
            die(mysqli_error(self::$link));
        
        return true;
        
    }
    
    public static function EditFrame($frame)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $t = "UPDATE frames SET name='%s', artifact_id='%d', content_type_id='%d' WHERE id='%d'";

        $query = sprintf($t, 
                         mysqli_real_escape_string(self::$link, $frame->Name),
                         mysqli_real_escape_string(self::$link, $frame->ArtifactId),
                         mysqli_real_escape_string(self::$link, $frame->ContentTypeId),
                         mysqli_real_escape_string(self::$link, $frame->Id));
        
        $result = mysqli_query(self::$link, $query);
        
        if (!$result)
            die(mysqli_error(self::$link));
        
        return true;
        
    }
    
    public static function NewTemplate($template)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $t = "INSERT INTO templates (id, name, path, description) VALUES ('%d', '%s', '%s', '%s')";

        $query = sprintf($t, 
                         mysqli_real_escape_string(self::$link, null),
                         mysqli_real_escape_string(self::$link, $template->name),
                         mysqli_real_escape_string(self::$link, $template->path),
                         mysqli_real_escape_string(self::$link, $template->description));
                
        $result = mysqli_query(self::$link, $query);
        
        if (!$result)
            die(mysqli_error(self::$link));
        
        return true;
        
    }
    
    /// <summary>
    /// Gets single frame by its id.
    /// </summary>
    /// <param name="$id_frame">The id of frame to get.</param>
    /// <returns>Single frame.</returns>
    public static function GetFrame($id_frame, $expName)
    {    
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM frames WHERE id=%d", (int)$id_frame);
        
        $frame = mysqli_fetch_assoc($result);
        return new FrameBase($frame['id'], $frame['name'], $expName, $frame['artifact_id'], $frame['content_type_id']);
    }

    /// <summary>
    /// Gets single artifact by its id.
    /// </summary>
    /// <param name="$id_artifact">The id of frame to get.</param>
    /// <returns>Single artifact.</returns>
    public static function GetArtifact($id_artifact)
    {    
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("SELECT * FROM artifacts WHERE id=%d", (int)$id_artifact);
        
        $artifact = mysqli_fetch_assoc($result);
        return new Artifact($artifact['id'], $artifact['name'], $artifact['content'], $artifact['description'], $artifact['content_type_id'], $artifact['templateId']);
    }
    
    /// <summary>
    /// Gets template by its id.
    /// </summary>
    /// <param name="$id_template">The id of template to get.</param>
    /// <returns>Single template.</returns>
    public static function GetTemplate($id_template)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();

        $result = self::GetQueryResult("SELECT * FROM templates WHERE id=%d", (int)$id_template);
        
        $template = mysqli_fetch_assoc($result);
        
        return new Template($template['id'], $template['name'], $template['path'], $template['description']);
    }
    
    public static function RemoveExposure($id_exposure)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("DELETE FROM exposures 
                           WHERE id=%d", (int)$id_exposure);
    }
    
    public static function RemoveArtifact($id_artifact)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("DELETE FROM artifacts 
                           WHERE id=%d", (int)$id_artifact);
    }
    
    public static function RemoveFrame($id_frame)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("DELETE FROM frames 
                           WHERE id=%d", (int)$id_frame);
    }
    
    public static function RemoveTemplate($id_template)
    {
        self::ConnectIfNeeded();
        self::ValidateConnection();
        
        $result = self::GetQueryResult("DELETE FROM templates 
                           WHERE id=%d", (int)$id_template);
    }
    
    private static function ConnectIfNeeded()
    {
        if (self::$link == null) self::Db_connect();
    }
    
    private static function ValidateConnection()
    {
        if(!mysqli_set_charset(self::$link, "utf8"))
        {
            printf("Error: ".mysqli_error(self::$link));
            return;
        }
    }
       
    private static function GetQueryResult($query, $param)
    {
        $dbquery = sprintf($query, (int)$param);
        $result = mysqli_query(self::$link, $dbquery);
        
        if(!$result)
        {
            die(mysqli_error(self::$link));
        }
        
        return $result;
    }
    
    private static function GetQueryStringResult($query, $param)
    {
        $dbquery = sprintf($query, mysqli_real_escape_string(self::$link, $param));
        $result = mysqli_query(self::$link, $dbquery);
        
        if(!$result)
        {
            die(mysqli_error(self::$link));
        }
        
        return $result;
    }
}

?>