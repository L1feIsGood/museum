<?php

    class FrameBase
    {
        public $Id;
        public $Name;
        public $ExpName;
        public $FrameNotFoundContent;
        public $FrameNotFoundContentPreview;
        public $ArtifactId;
        public $ArtifactName;
        public $Artifact;
        public $ContentTypeId;
        public $ContentTypeName;
        public $Content;
        public $Attributes;
        public $AttributeTag;
        
        function FrameBase($id, $name, $expName, $artifactId, $contentTypeId)
        {
            $this->Id = $id;
            $this->Name = $name;
            $this->ArtifactId = $artifactId;
            $this->ArtifactName = DbHandler::GetArtifactName($artifactId);
            $this->ContentTypeId = $contentTypeId;
            $this->ContentTypeName = DbHandler::GetContentTypeName($contentTypeId);
            $this->FrameNotFoundContent = "<p>Missing frame " . $name . "</p>";
            $this->ExpName = $expName;
            $this->FrameNotFoundContentPreview = "<a href=\"../choose_frame.php?name=".$this->Name."&amp;expname=" . $expName . "\"> <p>Missing frame " . $this->Name . "</p> </a>";
            $this->Artifact = DbHandler::GetArtifact($this->ArtifactId)
                or die("Failed to initialize frame " . $this->Name);
        }
        
        function SetExternalAttributes($attributes)
        {
            $this->Attributes = $attributes;
        }
        
        function InitContent($frame)
        {
            $this->Content = "";
            
            if ($this->ContainsAtrTag($frame))
            {
                $this->InitAtrFrame();
                return;
            }
            switch ($this->ContentTypeId)
            {
                case 1:
                    $this->InitTextContent($frame);
                    break;
                case 2:
                    $this->InitImageContent($frame);
                    break;
            }
        }
        
        function InitContentForPreview($frame)
        {
            $this->Content = "";
            
            if ($this->ContainsAtrTag($frame))
            {
                $this->InitAtrFrame();
                return;
            }
            switch ($this->ContentTypeId)
            {
                case 1:
                    $this->InitTextContentForPreview($frame);
                    break;
                case 2:
                    $this->InitImageContentForPreview($frame);
                    break;
            }
        }
        
        function ContainsAtrTag($frame)
        {
            foreach($frame->Attributes as $attribute)
            {
                if ($attribute->name == "atr") 
                {
                    $this->AttributeTag = $attribute->value;
                    
                    return true;
                }
            }
            return false;
        }
        
        function InitAtrFrame()
        {
            switch($this->AttributeTag)
            {
                case "art-title":
                    $this->Content = "<p>" . $this->Artifact->Name . "</p>";
                    break;
                case "art-descrip":
                    $this->Content = "<p>" . $this->Artifact->Description . "</p>";
                    break;
                default:
                    $this->Content = "<p>Attribute not supported... </p>";
                    break;
            }
        }
        
        function InitTextContent($frame)
        {
            $this->Content = "<p " ;
            foreach($frame->Attributes as $attribute)
            {
                if($attribute->name != "name")
                    $this->Content .= " " . $attribute->name . "=\"" . $attribute->value . "\"";
            }
            $this->Content .= ">" . $this->Artifact->Content . "</p>";
        }
        
        function InitImageContent($frame)
        {
            $this->Content = "<img src=\"" . $this->Artifact->Content . "\"";
            foreach($frame->Attributes as $attribute)
            {
                if($attribute->name != "name")
                    $this->Content .= " " . $attribute->name . "=\"" . $attribute->value . "\"";
            }
            $this->Content .= "/>";
        }
        
        function InitTextContentForPreview($frame)
        {
            $this->Content = "<a href=\"../choose_frame.php?name=".$this->Name."&amp;expname=" . $this->ExpName . "\"><p " ;
            foreach($frame->Attributes as $attribute)
            {
                if($attribute->name != "name")
                    $this->Content .= " " . $attribute->name . "=\"" . $attribute->value . "\"";
            }
            $this->Content .= ">" . $this->Artifact->Content . "</p></a>";
        }
        
        function InitImageContentForPreview($frame)
        {
            $this->Content = "<img src=\"" . $this->Artifact->Content . "\"";
            foreach($frame->Attributes as $attribute)
            {
                if($attribute->name != "name")
                    $this->Content .= " " . $attribute->name . "=\"" . $attribute->value . "\"";
            }
            $this->Content .= "/>";
        }

    }

?>