<?php

    interface IPreviewable
    {
        public function GetPreview();
    }
?>