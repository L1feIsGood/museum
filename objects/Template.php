<?php

    class Template 
    {
        public $id;
        public $name;
        public $path;
        public $content;
        public $description;
        public $doc;
        public $xml;
                
        function Template($id, $name, $path, $description)
        {
            $this->id = $id;
            $this->name = $name;
            $this->path = $path;
            if ($description == null) $this->description = "";
            else $this->description = $description;
            //echo "name = " . $name . "</br>" . "path = " . $path . "</br>" . "description = " . $description . "</br>" . "id = " . $id . "</br>";
            if ($this->path != "")
            {
                
                $this->doc = new DOMDocument();
                $this->doc->load($this->path)
                    or die("Failed to open template " . $this->name);
                
                $this->xml = simplexml_load_file($this->path)
                    or die("Failed to open template " . $this->name);
            }
            else 
            {
                echo "<script>alert('Failed to open template');</script>";
            }
            
        }
        
        public function Show()
        {
            echo $this->doc->saveXML();
        }
        
        /// <summary>
        /// Parses template xml and extracts frames.
        /// </summary>
        /// <returns>Array of frames.</returns>
        public function GetFrames($expName)
        {            
            // Gets array of DOM objects
            $elements = $this->doc->getElementsByTagName('*');
            $frames = array();
            foreach($elements as $element)
            {
                $attributes = $element->attributes;
                // Adds frames to array according to their type
                if ($element->tagName == "frame")
                {
                    $frame = new FrameBase("", $element->getAttribute("name"), $expName, "", "");
                    $frame->SetExternalAttributes($attributes);
                    $frames[] = $frame;
                }
            }
            return $frames;
        }
        
        /// <summary>
        /// Parses template xml and extracts frames.
        /// </summary>
        /// <returns>Void.</returns>
        public function Replace($frameName, $frameContent)
        {
            $elements = $this->doc->getElementsByTagName('*');
    
            foreach($elements as $element)
            {
                //echo $element->tagName."</br>";
                
                if ($element->getAttribute("name") == $frameName)
                {
                    $framedoc = new DOMDocument;
                    $framedoc->loadXML($frameContent);
                    
                    $element->parentNode->replaceChild($this->doc->importNode($framedoc->documentElement, true), $element);
            //echo "</br></br>";
                    return;
                }
            }
            
        }
    }
?>