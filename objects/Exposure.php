<?php

    class Exposure implements IPreviewable
    {
        public $id;
        public $name;
        public $author;
        public $annotation;
        public $creationDate;
        public $likes;
        public $dislikes;
        public $previewImgSrc;
        public $mainImgSrc;
        public $template;
        public $templateId;
        public $expFrames;
        private $framesId;
        public $arrFramesId;
        
        function Exposure($id, $name, $author, $annotation, $creationDate, $likes, $dislikes, $previewImgSrc, $mainImageSrc, $templateId, $framesIds)
        {
            $this->id = $id;
            $this->name = $name;
            $this->author = $author;
            $this->annotation = $annotation;
            $this->creationDate = $creationDate;
            $this->likes = $likes;
            $this->dislikes = $dislikes;
            $this->previewImgSrc = $previewImgSrc;
            $this->mainImgSrc = $mainImageSrc;
            
            $this->template = DbHandler::GetTemplate($templateId);
            $this->templateId = $templateId;
            
            $this->framesId = json_decode($framesIds);
            //Берем из базы фреймы по id
            $this->expFrames = array();
            $this->arrFramesId = array();
            
            // Костыль с инициализацией предпросмотра при выборе шаблона
            if(count($this->framesId)>0)
            {
                foreach($this->framesId as $frameId)
                {
                    $this->expFrames[] = DbHandler::getFrame($frameId, $name);
                    $this->arrFramesId[] = $frameId;
                }
            }
        }

        /// <summary>
        /// IPreviewable interface implementation (exposition image/link on landing page)
        /// </summary>
        /// <returns>Void</returns>
        function GetPreview()
        {
            return $this->previewImgSrc;
        }
        
        function Liked()
        {
            $this->likes += 1;
        }
                        
        function Disliked()
        {
            $this->dislikes += 1;
        }
        
        /// <summary>
        /// Checks, if given frame is in current exposition.
        /// </summary>
        /// <param name="$frameName">The name of frame to look for.</param>
        /// <returns>Yes or no.</returns>
        function Contains($frameName)
        {
            
            foreach($this->expFrames as $expFrame)
            {
                if ($expFrame->Name == $frameName)
                {
                    return true;
                }
            }
            return false;
        }
        
        /// <summary>
        /// Gets frame by its name.
        /// </summary>
        /// <param name="$frameName">The name of frame to look for.</param>
        /// <returns>Frame from exposure.</returns>
        function GetFrame($frameName)
        {
            foreach($this->expFrames as $expFrame)
            {
                if ($expFrame->Name == $frameName)
                {
                    return $expFrame;
                }
            }
            return null;
        }
        
        /// <summary>
        /// Gets xml content of the frame.
        /// </summary>
        /// <param name="$frameName">The name of frame to look for.</param>
        /// <returns>Frame content (xml).</returns>
        function GetFrameContent($frameName)
        {
            foreach($this->expFrames as $expFrame)
            {
                if ($expFrame->Name == $frameName) return $expFrame->Content;
            }
        }
        
        function AddFrame($frameId)
        {
            $this->arrFramesId[] = $frameId;
        }
        
        /// <summary>
        /// Prepares exposition to be shown.
        /// </summary>
        /// <returns>Void</returns>
        function Show()
        {
            // Get the frame names from premade template
            $frames = $this->template->GetFrames($this->name);
            
            foreach($frames as $frame)
            {
                //echo "YES " . $frame->Name."</br>";
                // If exposition contains frame from template, replace its content
                if ($this->Contains($frame->Name))
                {
                    $expFrame = $this->GetFrame($frame->Name);
                    $expFrame->InitContent($frame);
                    $this->template->Replace($frame->Name, $this->GetFrameContent($frame->Name));
                }
                // Otherwise insert "not found frame" (aka 404)
                else 
                {
                    $this->template->Replace($frame->Name, $frame->FrameNotFoundContent);
                }
            }
            
            // Echoes template, filled with exposition frames
            $this->template->Show();
        }
        
        /// <summary>
        /// Prepares exposition to be shown.
        /// </summary>
        /// <returns>Void</returns>
        function Preview()
        {
            // Get the frame names from premade template
            $frames = $this->template->GetFrames($this->name);
            
            foreach($frames as $frame)
            {
                //echo "YES " . $frame->Name."</br>";
                // If exposition contains frame from template, replace its content
                if ($this->Contains($frame->Name))
                {
                    $expFrame = $this->GetFrame($frame->Name);
                    $expFrame->InitContentForPreview($frame);
                    $this->template->Replace($frame->Name, $this->GetFrameContent($frame->Name));
                }
                // Otherwise insert "not found frame" (aka 404)
                else 
                {
                    $this->template->Replace($frame->Name, $frame->FrameNotFoundContentPreview);
                }
            }
            
            // Echoes template, filled with exposition frames
            $this->template->Show();
        }
    }
?>