<?php
    spl_autoload_register(function ($pClassName) {
        include("objects" . "/" . $pClassName . ".php");
    });

    $template = DbHandler::GetTemplateByName($_GET['templateName']);
    $exposure = DbHandler::GetExposureByName($_GET['expname']);
    if ($exposure == null)
    {
                                //$id,    $name,          $author, $annotation, $creationDate, $likes, $dislikes, $previewImgSrc, $mainImageSrc, $templateId, $framesIds
        $exposure = new Exposure(null, $_GET['expname'], null, $_GET['annotation'], null, null, null, null, null, $template->id, null);
        DbHandler::NewExposure($exposure);
    }
    
    $action = "preview";
    include("views/exposure.php");
?>