<?php
    spl_autoload_register(function ($pClassName) {
        include("../objects" . "/" . $pClassName . ".php");
    });


    if(isset($_GET['action']))
    {
        $action = $_GET['action'];
    }
    else 
    {
        $action = "";
    }

    if (isset($_GET['object']))
        $object = $_GET['object'];
    else
        $object = null;

    switch ($action)
    {
        case "addto":
            $exposure = DbHandler::GetExposureByName($_GET['name']);
            if($exposure != null)
            {
                $exposure->Addframe($_POST['selectedFrame']);
                DbHandler::EditExposure($exposure);
            }
            
            $templates = DbHandler::GetTemplates();
            $expName = $_GET['name'];
            $expTemplate = $exposure->template->name;
            include("../views/add_exposure_admin.php");
            break;
        case "add":
            switch($object)
            {
                case "exposure":
                    if(!empty($_POST))
                    {
                        $exposure = DbHandler::GetExposureByName($_POST['name']);
                            if($_FILES["previewImg"]["size"] > 1024*3*1024)
                            {
                              echo ("Размер файла превышает три мегабайта");
                              exit;
                            }
                            // Проверяем загружен ли файл
                            if(is_uploaded_file($_FILES["previewImg"]["tmp_name"]))
                            {
                              
                              // Если файл загружен успешно, перемещаем его
                              // из временной директории в конечную
                              move_uploaded_file($_FILES["previewImg"]["tmp_name"], "../img/".$_FILES["previewImg"]["name"]);
                              $exposure->previewImgSrc = "img/" . $_FILES["previewImg"]["name"];
                            } 
                            else 
                            {
                               echo("Ошибка загрузки файла");
                            }
                        
                        
                            if($_FILES["mainImg"]["size"] > 1024*10*1024)
                            {
                              echo ("Размер файла превышает десять мегабайт");
                              exit;
                            }
                            // Проверяем загружен ли файл
                            if(is_uploaded_file($_FILES["mainImg"]["tmp_name"]))
                            {
                              
                              // Если файл загружен успешно, перемещаем его
                              // из временной директории в конечную
                              move_uploaded_file($_FILES["mainImg"]["tmp_name"], "../img/".$_FILES["mainImg"]["name"]);
                                $exposure->mainImgSrc = "img/" . $_FILES["mainImg"]["name"];
                            } 
                            else 
                            {
                               echo("Ошибка загрузки файла");
                            }
                        
                        
                        //$exposure->annotation = $_POST['annotation'];
                        //echo $_POST['annotation'];;
                        DbHandler::EditExposure($exposure);
                        header("Location: index.php");
                    }
                    $templates = DbHandler::GetTemplates();
                    $expName = "Enter name";
                    $expTemplate = null;
                    include("../views/add_exposure_admin.php");
                    break;
                    
                case "artifact":
                    if(!empty($_POST))
                    {
                        $contentType = DbHandler::GetContentTypeByName($_POST['contentType']);
                        switch ($contentType->Name)
                        {
                            case "image":
                                if($_FILES["filename"]["size"] > 1024*3*1024)
                                    {
                                      echo "<script>alert('Размер файла превышает три мегабайта');</script>";
                                      exit;
                                    }
                                    // Проверяем загружен ли файл
                                    if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
                                    {
                                      // Если файл загружен успешно, перемещаем его
                                      // из временной директории в конечную
                                      move_uploaded_file($_FILES["filename"]["tmp_name"], "/img/".$_FILES["filename"]["name"]);
                                        $artifact = new Artifact("", $_POST['name'], "img/".$_FILES["filename"]["name"], $_POST['description'], $contentType->Id, 1);
                                        DbHandler::NewArtifact($artifact);
                                    } 
                                    else 
                                    {
                                        echo "<script>alert('Ошибка загрузки файла');</script>";
                                    }
                                break;
                            case "text":
                                $artifact = new Artifact("", $_POST['name'], $_POST['text'], $contentType->Id, 1);
                                DbHandler::NewArtifact($artifact);
                                break;
                        }
                        header("Location: index.php");
                    }
                    $types = DbHandler::GetContentTypes();
                    include("../views/add_artifact_admin.php");
                    break;
                case "frame":
                    if(!empty($_POST))
                    {
                        $contentType = DbHandler::GetContentTypeByName($_POST['contentType']);
                        $frame = new FrameBase(null, $_POST['name'], $_POST['selectedArtifact'], $contentType->Id);
                        DbHandler::NewFrame($frame);
                        header("Location: index.php");
                    }
                    $types = DbHandler::GetContentTypes();
                    include("../views/add_frame_admin.php");
                    break;
            }
            break;
            
        case "edit":
            switch($object)
            {
                case "exposure":
                    if(!empty($_POST))
                    {
                        $exposure = new Exposure(null, $_POST['name'],null, null, null, null, null, null, 2, null);
                        //DbHandler::EditExposure($exposure);
                        header("Location: index.php");
                    }
                    $templates = DbHandler::GetTemplates();
                    include("../views/edit_exposure_admin.php");
                    break;
                    
                //case "artifact":
                //    $id = (int)$_GET['id'];
                //    if(!empty($_POST) && $id > 0)
                //    {
                //        $contentType = DbHandler::GetContentTypeByName($_POST['contentType']);
                //        $artifact = new Artifact($id, $_POST['name'], "", $contentType->Id, "");
                //        $id, $name, $content, $description, $contentTypeId, $templateId
                //        DbHandler::EditArtifact($artifact);
                //        header("Location: index.php");
                //    }
                //    $artifacts = DbHandler::GetArtifacts();
                //    $types = DbHandler::GetContentTypes();
                //    include("../views/edit_artifact_admin.php");
                //    break;
                case "frame":
                    $id = (int)$_GET['id'];
                    if(!empty($_POST) && $id > 0)
                    {
                        $contentType = DbHandler::GetContentTypeByName($_POST['contentType']);
                        
                        $frame = new FrameBase($id, $_POST['name'], $_POST['selectedArtifact'], $contentType->Id);
                        DbHandler::EditFrame($frame);
                        header("Location: index.php");
                    }
                    $types = DbHandler::GetContentTypes();
                    $frame = DbHandler::GetFrame($_GET['id']);
                    include("../views/edit_frame_admin.php");
                    break;
            }
            break;
        case "delete":
            header("Location: index.php");
            switch($object)
            {
                case "exposure":
                    DbHandler::RemoveExposure($_GET['id']);
                    break;
                case "artifact":
                    DbHandler::RemoveArtifact($_GET['id']);
                    break;
                case "frame":
                    DbHandler::RemoveFrame($_GET['id']);
                    break;
                case "template":
                    DbHandler::RemoveTemplate($_GET['id']);
                    break;
            }
            $exposures = DbHandler::GetExposures();
            $artifacts = DbHandler::GetArtifacts();
            $frames = DbHandler::GetFrames();
            $templates = DbHandler::GetTemplates();
            include("../views/main_admin.php");
            break;
        case "upload":
            //header("Location: index.php");
            switch($object)
            {
                case "template":
                        if($_FILES["filename"]["size"] > 1024*3*1024)
                        {
                          echo ("Размер файла превышает три мегабайта");
                          exit;
                        }
                        // Проверяем загружен ли файл
                        if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
                        {
                          
                          // Если файл загружен успешно, перемещаем его
                          // из временной директории в конечную
                          move_uploaded_file($_FILES["filename"]["tmp_name"], "../templates/".$_FILES["filename"]["name"]);
                          $template = new Template(null, $_FILES["filename"]["name"], "../templates/".$_FILES["filename"]["name"], $_POST['description']);
                          DbHandler::NewTemplate($template);
                        } 
                        else 
                        {
                           echo("Ошибка загрузки файла");
                        }
                    break;
            }
            $exposures = DbHandler::GetExposures();
            $artifacts = DbHandler::GetArtifacts();
            $frames = DbHandler::GetFrames();
            $templates = DbHandler::GetTemplates();
            include("../views/main_admin.php");
            break;
        default:
            $exposures = DbHandler::GetExposures();
            $artifacts = DbHandler::GetArtifacts();
            $frames = DbHandler::GetFrames();
            $templates = DbHandler::GetTemplates();
            include("../views/main_admin.php");
            break;
    }
    
?>