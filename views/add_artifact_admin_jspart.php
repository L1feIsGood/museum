<?php
    spl_autoload_register(function ($pClassName) {
        include("../objects" . "/" . $pClassName . ".php");
    });

    $contentType = DbHandler::GetContentTypeByName($_GET['contentTypeName']);

    //$contentType = DbHandler::GetContentTypeByName($_GET['contentType']);
    //$artifacts = DbHandler::GetArtifactsByContentType($contentType->Id);

    switch ($contentType->Name)
    {
        case "text":
            include('../html_parts/artifact_content_text.php');
            break;
        case "image":
            include('../html_parts/artifact_content_image.php');
            break;
    }
?>
