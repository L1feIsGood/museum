<!DOCTYPE html>
<html>
    <head>
        
        <script>  
            function showContent(link) {  
        
                var cont = document.getElementById('contentBody');  
                var loading = document.getElementById('loading');  
        
                cont.innerHTML = loading.innerHTML;  
                
                var selectBox = document.getElementById("content_type");
                link = link + selectBox.options[selectBox.selectedIndex].value;
                
                var http = createRequestObject();  
                if( http )   
                {  
                    http.open('get', link);  
                    http.onreadystatechange = function ()   
                    {  
                        if(http.readyState == 4)   
                        {  
                            cont.innerHTML = http.responseText;  
                        }  
                    }  
                    http.send(null);      
                }  
                else   
                {  
                    document.location = link;  
                }  
            }  
        
            // создание ajax объекта  
            function createRequestObject()   
            {  
                try { return new XMLHttpRequest() }  
                catch(e)   
                {  
                    try { return new ActiveXObject('Msxml2.XMLHTTP') }  
                    catch(e)   
                    {  
                        try { return new ActiveXObject('Microsoft.XMLHTTP') }  
                        catch(e) { return null; }  
                    }  
                }  
            }  
        </script>
        
		<title>Admin add artifact</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="../css/style.css">
    </head>
    <body>
            <h1 id="text-exposition">Administrator panel</h1>
        <div id="content">
            <div id="main-holst">
                <p id="text-exposition">New artifact</p>
                <?php
                    switch ($action)
                    {
                        case "add": ?>
                            <form method="post" action="index.php?action=add&object=artifact" enctype="multipart/form-data">
                            <label>
                                Name
                                <input type="text" name="name" class="form-item" autofocus required>
                            </label></br>
                            <label>
                                Description
                                <input type="text" name="description" class="form-item" required>
                            </label></br>
                            <label>
                                Content type
                                <select id="content_type" name="contentType" onChange="showContent('../views/add_artifact_admin_jspart.php?contentTypeName=');">
                                <option value="Choose content type" selected>Choose content type</option>
                                <?php foreach($types as $type): 
                                    echo $type->name ?>
                                    <option value=<?=$type->Name?>><?=$type->Name?></option>
                                <?php endforeach?>
                                </select>
                            </label></br>
                            <label name="selectedtype"></label></br>
                            <div id="contentBody">  
                            </div>  
                            <div id="loading" style="display: none">  
                                Идет загрузка...  
                            </div> 
                            <input type="submit" value="save" class="btn">
                            </form>
                <?php
                            break;
                        case "edit":
                            break;
                        case "delete":
                            break;
                    }
                ?>
            </div>
        </div>
    </body>
</html>