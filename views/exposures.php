<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<title>Виртуальный музей - Главная страница</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<!--Подключение css-->
		<link rel="stylesheet" href="libs/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/fonts.css">
		<!-- END Подключение css-->
	</head>
    <body>
        <div id="mainBox">
        <div id="header">
				<div id="imges" class="">
					<div class="row">
						<div class="col-sm-4">
							<img src="img/svg/title-logo.svg" alt="Виртуальный музей НИУ ВШЭ-Пермь">
						</div>
						<div class="col-sm-1 col-sm-offset-6">
							<span>
								<img id="rss-img" src="img/svg/rss-button.svg" alt="Виртуальный музей НИУ ВШЭ-Пермь">
							</span>
						</div>
						<div class="col-sm-1">
							<span>
								<img id="search-img"src="img/svg/search-button.svg" alt="Виртуальный музей НИУ ВШЭ-Пермь">
							</span>
						</div>						
					</div>
				</div>
			</div>
             <a href ="admin">Administrator panel</a></br>
            <a href ="test">Test panel</a>
        <div id="content">
            <div class="carousel slide" id="slider">
						 <ol class="carousel-indicators">
                             <?php $i = 0; foreach($exposures as $exp): ?>
						  	    <li  <?php if ($i == 0): ?> class="active" <?php endif;?> data-target="#slider" data-slide-to="<?=$i;?>"/>
                             <?php $i++; endforeach ?>
						</ol>
					  	<div  class="carousel-inner">
                             <?php $i = 0; foreach($exposures as $exp): ?>
                                <div class="item<?php if ($i == 0): ?> active<?php endif;?>">
                                    <div class="carousel-caption ">
						  			     <div class="container-fluid">
						  				    <div class="row-fluid">
							  				   <div class="col-sm-7 caption-header"><p class=""><?=$exp->name?></p></div>
							  				   <div class="col-sm-5 caption-text"><p><?=$exp->annotation?></p></div>
						  				    </div>
						  			     </div>
					  			      </div>
					  			<img src="<?=$exp->mainImgSrc?>"/>
					  		    </div>
                             <?php $i++; endforeach ?>
					  	</div>
					  	<!--<a class="carousel-control left" data-slide="prev" href="#slider">&lsaquo;</a>
					  	<a class="carousel-control right" data-slide="next" href="#slider">&rsaquo;</a>-->
					</div>
            
            <div class="container">
				<div id="main-holst" >
					<div class="row">
							<div id="text-title" class="span4">
								Экспозиции 
							</div>
					</div>
                    
                <?php $i = 0; foreach($exposures as $exp): ?> 
                <?php if ($i % 3 == 0): ?> <div class="row-fluid"> <?php endif;?>
					<div class="effect eff-2-2 col-sm-5">
						<div class="card" style="background-color:#b99481;">
							<img class="pic" src="<?=$exp->previewImgSrc?>">
							<br><p id="down-text"><?=$exp->name?></p>
						</div>	
						<div class="caption">
							<h4><?=$exp->name?></h4>
							<p><?=$exp->annotation?></p>
							<a class="btn" href ="exposure.php?id=<?=$exp->id?>" title="Узнать больше">Узнать больше <img class="flip-horizontal" src="img/svg/arrow.svg"></img></a>
						</div>
					</div>
                <?php if ($i % 3 == 0): ?> </div> <?php $i++; endif;?>
                <?php endforeach ?>
                
                <div style="clear:both"></div>
            </div>
            </div>
        </div>
    </div>

        <!--Подключение js-->
		<script type="text/javascript" src="libs/jquery/jquery-2.2.3.min.js"></script>
		<script type="text/javascript" src="libs/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/common.js"></script>
		<!-- END Подключение js-->
    </body>
</html>