<!DOCTYPE html>
<html>
    <head>
        <script>  
            function showContent(link) {  
        
                var cont = document.getElementById('contentBody');  
                var loading = document.getElementById('loading');  
        
                cont.innerHTML = loading.innerHTML;  
                
                var selectBox = document.getElementById("type");
                link = link + selectBox.options[selectBox.selectedIndex].value;
                
                var http = createRequestObject();  
                if( http )   
                {  
                    http.open('get', link);  
                    http.onreadystatechange = function ()   
                    {  
                        if(http.readyState == 4)   
                        {  
                            cont.innerHTML = http.responseText;  
                        }  
                    }  
                    http.send(null);      
                }  
                else   
                {  
                    document.location = link;  
                }  
            }  
        
            // создание ajax объекта  
            function createRequestObject()   
            {  
                try { return new XMLHttpRequest() }  
                catch(e)   
                {  
                    try { return new ActiveXObject('Msxml2.XMLHTTP') }  
                    catch(e)   
                    {  
                        try { return new ActiveXObject('Microsoft.XMLHTTP') }  
                        catch(e) { return null; }  
                    }  
                }  
            }  
        </script>
        
        
		<title>Admin edit artifact</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="../css/style.css">
    </head>
    <body>
            <h1 id="text-exposition">Administrator panel</h1>
        <div id="content">
            <div id="main-holst">
                <p id="text-exposition">Edit artifact</p>
                <form method="post" action="index.php?action=edit&object=artifact&id=<?=$_GET['id']?>">
                    <label id="text-exposition">
                        Name
                        <input type="text" name="name" value="<?=$frame->Name;?>" class="form-item" autofocus required>
                    </label>
                    </br>
                    <label id="text-exposition">
                        Content type
                        <select id="type" name="contentType" onChange="showContent('../views/add_frame_admin_jspart.php?contentType=');">
                            <?php foreach($types as $type):?>
                            <option value=<?=$type->Name?> <?php if ($type->Name == $frame->ContentTypeName): ?> selected <?php endif;?>><?=$type->Name?></option>
                            <?php endforeach?>
                        </select>
                    </label>
                    </br>
                    </br>
                    <div id="loading" style="display: none">  
                        Идет загрузка...  
                    </div>  
                    <div id="contentBody">  
                    </div>  
  
                    <input type="submit" value="save" class="btn">
        
                </form>
            </div>
        </div>
    </body>
</html>