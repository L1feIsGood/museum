<?php
    spl_autoload_register(function ($pClassName) {
        include("../objects" . "/" . $pClassName . ".php");
    });
    $contentType = DbHandler::GetContentTypeByName($_GET['contentType']);
    $artifacts = DbHandler::GetArtifactsByContentType($contentType->Id);


    include('../html_parts/artifacts_table.php');
?>
