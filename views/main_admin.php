<!DOCTYPE html>
<html>
    <head>
		<title>HSE Museum test admin</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="../css/style.css">
    </head>
    <body>
            <h1 id="text-exposition">Administrator panel</h1>
        <div id="content">
	<div id="main-holst">
            <div>
                <p id="text-exposition">Expositions</p>
            </div>
            <div>
                <a id="text-exposition" href ="index.php?action=add&object=exposure">Add</a>
            </div>
            <div>
                <table class="admin-table">
                    <tr>
                        <th id="text-exposition">Date</th>
                        <th id="text-exposition">Header</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <?php foreach($exposures as $exp): ?>
                    <tr>
                        <td id="table-text"><?=$exp->creationDate?></td>
                        <td id="table-text"><?=$exp->name?></td>
                        <td>
                            <a id="text-exposition" href="index.php?action=edit&object=exposure&id=<?=$exp->id?>">Edit</a>
                        </td>
                        <td>
                            <a id="text-exposition" href="index.php?action=delete&object=exposure&id=<?=$exp->id?>">Delete</a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>
            
    <div id="main-holst">
            <div>
                <p id="text-exposition">Artifacts</p>
            </div>
            <div>
                <a id="text-exposition" href ="index.php?action=add&object=artifact">Add</a>
            </div>
            <div>
                <table class="admin-table">
                    <tr>
                        <th id="text-exposition">Name</th>
                        <th id="text-exposition">Content type</th>
                        <th id="text-exposition">Content</th>
                        <th id="text-exposition">Description</th>
                        <th></th>
                    </tr>
                    <?php foreach($artifacts as $art): ?>
                    <tr>
                        <td id="table-text"><?=$art->Name?></td>
                        <td id="table-text"><?=$art->ContentTypeName?></td>
                        <td id="table-text"><?=$art->GetPreview()?></td>
                        <td id="table-text"><?=$art->Description?></td>
                        <!--<td>
                            <a id="text-exposition" href="index.php?action=edit&object=artifact&id=<?=$art->Id?>">Edit</a>
                        </td>-->
                        <td>
                            <a id="text-exposition" href="index.php?action=delete&object=artifact&id=<?=$art->Id?>">Delete</a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>
            
    <div id="main-holst">
            <div>
                <p id="text-exposition">Frames</p>
            </div>
            <div>
                <a id="text-exposition" href ="index.php?action=add&object=frame">Add</a>
            </div>
            <div>
                <table class="admin-table">
                    <tr>
                        <th id="text-exposition">Name</th>
                        <th id="text-exposition">Content type</th>
                        <th id="text-exposition">Artifact </th>
                        <th></th>
                    </tr>
                    <?php foreach($frames as $frame): ?>
                    <tr>
                        <td id="table-text"><?=$frame->Name?></td>
                        <td id="table-text"><?=$frame->ContentTypeName?></td>
                        <td id="table-text"><?=$frame->ArtifactName?></td>
                        <td>
                            <a id="text-exposition" href="index.php?action=edit&object=frame&id=<?=$frame->Id?>">Edit</a>
                        </td>
                        <td>
                            <a id="text-exposition" href="index.php?action=delete&object=frame&id=<?=$frame->Id?>">Delete</a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>        
           
    <div id="main-holst">
            <div>
                <p id="text-exposition">Templates</p>
            </div>
            <div>
                <p id="text-exposition"><b> Upload template </b></p></h2>
                <form action="index.php?action=upload&object=template" method="post" enctype="multipart/form-data">
                <input type="file" name="filename"><br> 
                <input type="text" name="description" value="description"><br> 
                <input type="submit" value="Upload"><br>
                </form>
            </div>
            <div>
                <table class="admin-table">
                    <tr>
                        <th id="text-exposition">Name</th>
                        <th id="text-exposition">Description </th>
                        <th></th>
                    </tr>
                    <?php foreach($templates as $template): ?>
                    <tr>
                        <td id="table-text"><?=$template->name?></td>
                        <td id="table-text"><?=$template->description?></td>
                        <td>
                            <a id="text-exposition" href="index.php?action=delete&object=template&id=<?=$template->id?>">Delete</a>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div> 
            
            </div>
    </body>
</html>