<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
<!--<![endif]-->
<head>
	
	<script>  
            function showContent(link) {  
        
                var cont = document.getElementById('contentBody');  
                var loading = document.getElementById('loading');  
        
                cont.innerHTML = loading.innerHTML;  
                
                var selectBox = document.getElementById("temp");
                link = link + selectBox.options[selectBox.selectedIndex].value;
                link = link + "&expname=" + document.getElementById('name').value;  
                link = link + "&annotation=" + document.getElementById('annotation').value;  
                    
                var http = createRequestObject();  
                if( http )   
                {  
                    http.open('get', link);  
                    http.onreadystatechange = function ()   
                    {  
                        if(http.readyState == 4)   
                        {  
                            cont.innerHTML = http.responseText;  
                        }  
                    }  
                    http.send(null);      
                }  
                else   
                {  
                    document.location = link;  
                }  
            }  
            // создание ajax объекта  
            function createRequestObject()   
            {  
                try { return new XMLHttpRequest() }  
                catch(e)   
                {  
                    try { return new ActiveXObject('Msxml2.XMLHTTP') }  
                    catch(e)   
                    {  
                        try { return new ActiveXObject('Microsoft.XMLHTTP') }  
                        catch(e) { return null; }  
                    }  
                }  
            }  
        </script>


	<meta charset="utf-8" /> 
    <title>Заголовок</title> 
    <meta name="description" content="" /> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /> 
    <link rel="stylesheet" href="../libs/bootstrap/bootstrap-grid-3.3.1.min.css" /> 
    <link rel="stylesheet" href="../libs/bootstrap/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="../css/fonts.css" /> 
    <link rel="stylesheet" href="../css/main.css" /> 
    <link rel="stylesheet" href="../css/media.css" />
</head>
<body>
	<div id="mainBox" class="exposition container-fluid">
		<div class="row">
			<div id="header" class="col-sm-12 col-xs-12">
				<div class="container">
					<div class="row">
						<div class="col-sm-3"><a href="index.php"><span><img src="../img/svg/arrow.svg"></img>Administrator panel</span></a></div>
						<div class="col-sm-9 col-xs-9"><p>New exposure</p></div>
					</div>
				</div>
			</div>
            
        <div>
            <form method="post" action="index.php?action=add&object=exposure" enctype="multipart/form-data">
                <div class="effect col-sm-6 text-exposition">
                    <label>
                        <p>Name</p>
                        <input id="name" type="text" name="name" value="<?=$expName?>" class="form-item" autofocus required>
                    </label>
                    <label>
                        <p>Annotation</p>
                        <input id="annotation" type="text" name="annotation" value="" class="form-item" required>
                    </label>
                    <label>
                        <p>Preview image</p>
                        <input type="file" name="previewImg" accept="image/jpeg,image/png,image/gif"><br> 
                    </label>
                    <label>
                        <p>Main Image</p>
                        <input type="file" name="mainImg" accept="image/jpeg,image/png,image/gif"><br> 
                    </label>
                    
                </div>
                <div class="effect col-sm-6 text-exposition">
                    <label>
                        <p>Template</p>
                        <select id="temp" name="template" onChange="showContent('../preview_exposure.php?templateName=');">
                            <option value="Choose template" <?php if (null == $expTemplate): ?> selected <?php endif;?>>Choose template</option>
                            <?php foreach($templates as $template): 
                            echo $template->name ?>
                            <option value=<?=$template->name?> <?php if ($template->name == $expTemplate): ?> selected <?php endif;?> ><?=$template->name?></option>
                            <?php endforeach?>
                        </select>
                    </label>
                </div>  
                <div class="effect col-sm-6 text-exposition">
                    <input type="submit" value="save" class="btn">
                </div> 
            <div id="contentBody">  
            </div>  
  
            <div id="loading" style="display: none">  
                Идет загрузка...  
            </div>  
            </form>
            </div>
        </div>
    </div>
	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->
	<script src="../libs/jquery/jquery-1.11.1.min.js"></script>
	<script src="../libs/jquery-mousewheel/jquery.mousewheel.min.js"></script>
	<script src="../libs/fancybox/jquery.fancybox.pack.js"></script>
	<script src="../libs/waypoints/waypoints-1.6.2.min.js"></script>
	<script src="../libs/scrollto/jquery.scrollTo.min.js"></script>
	<script src="../libs/owl-carousel/owl.carousel.min.js"></script>
	<script src="../libs/countdown/jquery.plugin.js"></script>
	<script src="../libs/countdown/jquery.countdown.min.js"></script>
	<script src="../libs/countdown/jquery.countdown-ru.js"></script>
	<script src="../libs/landing-nav/navigation.js"></script>
	<script src="../js/common.js"></script>
	<!-- Yandex.Metrika counter --><!-- /Yandex.Metrika counter -->
	<!-- Google Analytics counter --><!-- /Google Analytics counter -->
</body>
</html>
