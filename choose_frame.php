<?php
    spl_autoload_register(function ($pClassName) {
        include("objects" . "/" . $pClassName . ".php");
    });
    
    $frames = DbHandler::GetFramesByName($_GET['name']);
    $expName = $_GET['expname'];
    include("choose_frame.html");
?>