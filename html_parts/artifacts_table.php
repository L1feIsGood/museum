<?php if (count($artifacts) == 0): ?> <p class="text-exposition">No such artifact</p> 
<?php else: ?>
<table class="admin-table">
                    <tr>
                        <th id="text-exposition"></th>
                        <th id="text-exposition">Name</th>
                        <th id="text-exposition">Content type</th>
                        <th id="text-exposition">Content</th>
                        <th id="text-exposition">Description</th>
                        <th></th>
                    </tr>
                    <?php foreach($artifacts as $art): ?>
                    <tr>
                        <td>
                            <input type="radio" name="selectedArtifact" value="<?=$art->Id?>"/>
                        </td>
                        <td id="table-text"><?=$art->Name?></td>
                        <td id="table-text"><?=$art->ContentTypeName?></td>
                        <td id="table-text"><?=$art->GetPreview()?></td>
                        <td id="table-text"><?=$art->Description?></td>
                    </tr>
                    <?php endforeach ?>
</table>
<?php endif;?>