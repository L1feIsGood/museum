<?php if (count($frames) == 0): ?> <p class="text-exposition">No such frame</p> 
<?php else: ?>
<table class="admin-table">
                    <tr>
                        <th id="text-exposition"></th>
                        <th id="text-exposition">Name</th>
                        <th id="text-exposition">Content type</th>
                        <th id="text-exposition">Artifact </th>
                        <th></th>
                    </tr>
                    <?php foreach($frames as $frame): ?>
                    <tr>
                        <td>
                            <input type="radio" name="selectedFrame" value="<?=$frame->Id?>"/>
                        </td>
                        <td id="table-text"><?=$frame->Name?></td>
                        <td id="table-text"><?=$frame->ContentTypeName?></td>
                        <td id="table-text"><?=$frame->Artifact->GetLocalPreview()?></td>
                    </tr>
                    <?php endforeach ?>
</table>
<?php endif;?>