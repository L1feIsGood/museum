<?php
    spl_autoload_register(function ($pClassName) {
        include("objects" . "/" . $pClassName . ".php");
    });

    $exposure = DbHandler::GetExposure($_GET['id']);
    include("exposure_show.php");
?>